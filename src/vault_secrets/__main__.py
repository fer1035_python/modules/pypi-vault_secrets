#!/usr/bin/env python3
# -*- coding: latin-1 -*-

"""
Make API calls to Vault Secrets to retrieve secrets.

Prerequisites:
    - Vault Secrets Organization ID
    - Vault Secrets Project ID
    - Vault Secrets Application Name
    - Vault Secrets Organization Client ID
    - Vault Secrets Organization Client Secret

Optional:
    - Secret Name (to return the latest value of a specific secret)

Organization and Project IDs can be retrieved using the vlt CLI:
https://developer.hashicorp.com/vault/tutorials/hcp-vault-secrets-get-started/hcp-vault-secrets-retrieve-secret
"""
import __init__

if __name__ == "__main__":
    __init__.main()
